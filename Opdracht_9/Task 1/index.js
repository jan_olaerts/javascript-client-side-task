const btn10 = document.querySelector("#btn10");
const task9aDiv = document.querySelector("#task9aDiv");
const area = document.querySelector("#area");
const popup = document.querySelector("#popup");
const close = popup.querySelector("strong");

let task9aSelected = false;

area.addEventListener("mouseup", (e) => {

  const posX = e.clientX -250;
  const posY = e.clientY -125;
  
  popup.style.display = "block";
  popup.style.left = `${posX}px`;
  popup.style.top = `${posY}px`;
});

btn10.addEventListener("click", () => {
  task9aSelected = !task9aSelected;

  if (task9aSelected) {
    addLine("Showing task 9 (1/2)");
    task9aDiv.style.display = "block";
  } else {
    task9aDiv.style.display = "none";
  }
})

close.addEventListener("click", () => {
  popup.style.display = "none";
})