const btn11 = document.querySelector("#btn11");
let colorChangeActive = false;

window.addEventListener("keyup", e => {
  if (colorChangeActive) {
    if(e.keyCode === 82) body.style.backgroundColor = "red";
    if(e.keyCode === 71) body.style.backgroundColor = "green";
    if(e.keyCode === 66) body.style.backgroundColor = "blue";
  } else return;

});

btn11.addEventListener("click", () => {
  colorChangeActive = !colorChangeActive;

  if(colorChangeActive) {
    addLine("Changing background color is active");
    addLine("Press 'r' to change the background color to red");
    addLine("Press 'g' to change the background color to green");
    addLine("Press 'b' to change the background color to blue");
    addLine("Press the 'Task 9 (2/2)' button to deactivate color changing");
  } else {
    addLine("Changing colors is not activated");
    addLine("Press the 'Task 9 (2/2)' button to activate color changing");
  }

})