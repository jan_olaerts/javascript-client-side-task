const btn19 = document.querySelector("#btn19");
const task15Div = document.querySelector("#task15Div");
const h2 = document.createElement("h2");
let task15Selected = false;
let timer;

function getCurrentTime() {
  const now = new Date();
  const hours = now.getHours();
  const minutes = now.getMinutes();
  const seconds = now.getSeconds();
  displayTime(hours, minutes, seconds);
}

function displayTime(hours, minutes, seconds) {
  if (hours < 10) hours = "0" + hours;
  if (minutes < 10) minutes = "0" + minutes;
  if (seconds < 10) seconds = "0" + seconds;

  h2.innerText = `It is ${hours}:${minutes}:${seconds}`;
  task15Div.appendChild(h2);
}

btn19.addEventListener("click", () => {
  task15Selected = !task15Selected;

  if (task15Selected) {
    addLine("Showing task 15");
    task15Div.style.display = "block";
    getCurrentTime();
    timer = setInterval(getCurrentTime, 1000);
  } else {
    task15Div.style.display = "none";
    clearInterval(timer);
  }
});