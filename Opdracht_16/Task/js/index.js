const body = document.querySelector("body");
const p = document.querySelector("#text");
const block = document.querySelector("#block");
let mouseIsDown = false;
let spaceDown = false;

function drag(e) {
  if (mouseIsDown) {
    posX = e.clientX - 50;
    posY = e.clientY - 50;

    block.style.left = `${posX}px`;
    block.style.top = `${posY}px`;
  } else if (spaceDown) {
    block.style.transform= "rotate(+45deg)";
    console.log(spaceDown);
  } else return;
}

window.addEventListener("mousedown", () => {
  mouseIsDown = true;
});

window.addEventListener("mouseup", () => {
  mouseIsDown = false;
})  

block.addEventListener("mousemove", drag);