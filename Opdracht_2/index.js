const btn2 = document.querySelector("#btn2");

const screenProps = {
  width: document.body.clientWidth,
  height: document.body.clientHeight,
  x: window.screenX,
  y: window.screenY
}

btn2.addEventListener("click", () => {
  addLine("Screen dimensions:");
  addLine(`Width: ${screenProps.width} Height: ${screenProps.height} x: ${screenProps.x} y: ${screenProps.y}`);
});