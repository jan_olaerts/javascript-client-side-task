const btn18 = document.querySelector("#btn18");
let task14bSelected = false;

btn18.addEventListener("click", () => {
  task14bSelected = !task14bSelected;

  if(task14bSelected) {
    task11Selected = false;
    task12Selected = false;

    formDiv.style.display = "block";
    form.removeAttribute("method");
    form.removeAttribute("action");
    addLine("Showing task 14 (2/2)");
    formTitle.innerText = "Task 14 (2/2)";
    cookieBtn.style.display = "block";
  } else {
    formDiv.style.display = "none";
  }
})