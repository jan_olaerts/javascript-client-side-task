const btn17 = document.querySelector("#btn17");
const task14aDiv = document.querySelector("#task14aDiv");
let task14aSelected = false;

let language = "null";
let buttons = task14aDiv.querySelectorAll("button");
buttons = Array.from(buttons);
buttons.forEach(b => b.addEventListener("click", setLanguage));

function setLanguage(e) {
  language = e.target.id;
  let name = "null";
  let content = "null";
  let url = "null";

  switch (language) {
    case "english":
      content = "english";
      url = "../Opdracht_14/Task 1/english.html";
      break;
    case "dutch":
      content = "dutch";
      url = "../Opdracht_14/Task 1/dutch.html";
      break;
    case "french":
      content = "french";
      url = "../Opdracht_14/Task 1/french.html";
      break;
  }

  setCookie("language", content, 0, 0, 1);
  window.open(url);
  return language;
}

btn17.addEventListener("click", () => {
  task14aSelected = !task14aSelected;

  if (task14aSelected) {
    task14aDiv.style.display = "block";
    addLine("Showing task 14 (1/2), after selecting a language click the button again twice to be redirected to the page");

    const cookie = getCookie("language");
    if(cookie === null) return;
    const language = cookie;
    let url = "null";
  
    if (language === "english")
      url = "../Opdracht_14/Task 1/english.html";
  
    else if (language === "dutch")
      url = "../Opdracht_14/Task 1/dutch.html";
  
    else if (language === "french")
      url = "../Opdracht_14/Task 1/french.html";
  
    window.open(url);
  } else {
    task14aDiv.style.display = "none";
  }

});