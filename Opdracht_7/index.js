const btn8 = document.querySelector("#btn8");

function showImage(time) {
  const hour = time[0];
  // const img = document.createElement("img");
  let src;

  if(hour > 6 && hour < 24)
    // img.setAttribute("src", "../Opdracht_7/img/sun.png");
    src = "../Opdracht_7/img/sun.png";

  else
    // img.setAttribute("src", "../Opdracht_7/img/moon.png");
    src = "../Opdracht_7/img/moon.png";

    addImage(src, 150);
}

btn8.addEventListener("click", () => {
  const time = getTime();
  greet(time);
  showImage(time);
});