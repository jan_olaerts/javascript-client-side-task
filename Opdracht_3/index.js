const btn3 = document.querySelector("#btn3");

btn3.addEventListener("click", () => {
  let name;
  do {
    name = prompt("Enter your name: ");
    if (!askStringNoNumbers(name)) alert("This is not a valid name!");
    if (name === "") alert("You did not give a name!");
    if (name === null) return;
  } while (!askStringNoNumbers(name) || name === "");

  let answer = confirm("Is your name correct? " + name);
  
  if(answer)
    alert("Hello " + name);
  
  else
    alert("No name");
})