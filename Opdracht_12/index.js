const btn14 = document.querySelector("#btn14");
let task12Selected = false;

btn14.addEventListener("click", () => {
  task12Selected = !task12Selected;

  if(task12Selected) {
    task11Selected = false;
    task14bSelected = false;

    formDiv.style.display = "block";
    form.setAttribute("method", "GET");
    form.action = "../Opdracht_12/Result.html";
    addLine("Showing task 12");
    formTitle.innerText = "Task 12";
    cookieBtn.style.display = "none";
  } else {
    formDiv.style.display = "none";
  }
})