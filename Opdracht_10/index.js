const btn12 = document.querySelector("#btn12");
const task10Div = document.querySelector("#task10Div");
let task10Selected = false;

const area1 = document.querySelector("#area1");
const area2 = document.querySelector("#area2");

const documentMouseMove = document.querySelector("#documentMouseMove");
const area1MouseMove = document.querySelector("#area1MouseMove");
const area2MouseMove = document.querySelector("#area2MouseMove");

const documentClick = document.querySelector("#documentClick");
const area1Click = document.querySelector("#area1Click");
const area2Click = document.querySelector("#area2Click");

document.addEventListener("mousemove", (e) => {
  console.log("Document mouse moved");
  documentMouseMove.innerText = "Document mouse moved";
});

area1.addEventListener("mousemove", (e) => {
  e.stopPropagation();
  console.log("Area1 mouse moved");
  area1MouseMove.innerText = "Area1 mouse moved";
})

area2.addEventListener("mousemove", (e) => {
  e.stopPropagation();
  console.log("Area2 mouse moved");
  area2MouseMove.innerText = "Area2 mouse moved";
})

document.addEventListener("mouseLeave", (e) => {
  documentMouseMove.innerText = "";
});

area1.addEventListener("mouseleave", (e) => {
  e.stopPropagation();
  area1MouseMove.innerText = "";
})

area2.addEventListener("mouseleave", (e) => {
  e.stopPropagation();
  area2MouseMove.innerText = "";
})

document.addEventListener("click", (e) => {
  console.log("Document clicked");
  documentClick.innerText = "Document clicked";
});

area1.addEventListener("click", (e) => {
  e.stopPropagation();
  console.log("Area1 clicked");
  area1Click.innerText = "Area1 clicked";
})

area2.addEventListener("click", (e) => {
  e.stopPropagation();
  console.log("Area2 clicked");
  area2Click.innerText = "Area2 clicked";
})

btn12.addEventListener("click", () => {
  task10Selected = !task10Selected;
  if(task10Selected) {
    task10Div.style.display = "block";
    addLine("Showing task 10, open the console the see changes");
  } else {
    task10Div.style.display = "none";
  }
})