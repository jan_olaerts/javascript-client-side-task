const btn5 = document.querySelector("#btn5");

btn5.addEventListener("click", () => {
  const cookiesEnabled = navigator.cookieEnabled;

  if (cookiesEnabled) {
    addLine("Cookies are enabled, you should not be redirected");
  } else {
    addLine("Disable cookies to be redirected");
    if(!navigator.cookieEnabled)
    location = "../Opdracht_4/Task 2/index.html";
  }
  
})