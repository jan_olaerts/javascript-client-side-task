const btn4 = document.querySelector("#btn4");

btn4.addEventListener("click", () => {
  addLine("Browser properties:");
  addLine(`Name of the browser: ${navigator.appName}`);
  addLine(`Browser version: ${navigator.appVersion}`);
  addLine(`Online: ${navigator.onLine}`);
  addLine(`Cookies enabled: ${navigator.cookieEnabled}`);
})