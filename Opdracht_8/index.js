const body = document.querySelector("body");
const btn9 = document.querySelector("#btn9");

function getHour() {
  const now = new Date();

  const h = now.getHours();
  const m = now.getMinutes();
  const s = now.getSeconds();

  const p = document.createElement("p");
  p.innerText = `It is ${(h < 9) ? '0' + h : h}:${(m < 9) ? '0' + m : m}:${(s < 9) ? '0' + s : s}`;
  c.appendChild(p);
  return h;
}

function addStyle(hour) {
  let color;

  if (hour < 6 || hour >= 22) {
    body.setAttribute("class", "night");
    color = "dark blue";
  }

  else if (hour >= 6 && hour < 8) {
    body.setAttribute("class", "morning");
    color = "dark orange";
  }

  else if (hour >= 8 && hour < 10) {
    body.setAttribute("class", "before-noon");
    color = "golden rod";
  }

  else if (hour >= 10 && hour < 16) {
    body.setAttribute("class", "after-noon");
    color = "green yellow";
  }

  else if (hour >= 16 && hour < 22) {
    body.setAttribute("class", "evening");
    color = "light skyblue";
  }

  const p = document.createElement("p");
  p.innerText = `The background color should be ${color}\n`;
  c.appendChild(p);
}

btn9.addEventListener("click", () => {
  const hour = getHour();
  addStyle(hour);
})