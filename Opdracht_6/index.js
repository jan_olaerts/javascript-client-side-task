const btn7 = document.querySelector("#btn7");

function getTime() {

  const now = new Date();
  return [now.getHours(), now.getMinutes(), now.getSeconds()];
}

function greet(time) {
  const hour = time[0];

  let text;
  if(hour > 6 && hour < 12)
    text = "morning";

  else if(hour >= 12 && hour < 18)
    text = "afternoon";

  else if(hour >= 18 && hour < 24)
    text = "evening";

  else
    text = "night";

  const hours = time[0] < 9 ? "0" + time[0] : time[0];
  const minutes = time[1] < 9 ? "0" + time[1] : time[1];
  const seconds = time[2] < 9 ? "0" + time[2] : time[2];

  addLine(`It is ${hours}:${minutes}:${seconds}
  Good ${text}!`);
}

btn7.addEventListener("click", () => {
  const time = getTime();
  greet(time);
});