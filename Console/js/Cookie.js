// Creating a cookie

function setCookie(name, content, days = 0, hours = 0, minutes = 0) {
  const now = new Date();
  const exp = new Date(now.getTime() + (((((days * 24) + hours) * 60) + minutes) * 60000));
  const expiration = exp.toUTCString();
  name = encodeURIComponent(name);
  content = encodeURIComponent(content);
  document.cookie = `${name}=${content};expires=${expiration}`;
}

// Getting to content of a cookie

function getCookie(name) {
  const regex = new RegExp(name + "=([^;]+)");
  const result = regex.exec(document.cookie);
  if (result) return decodeURIComponent(result[1]);
  else return null;
}

// Clearing the cookie

function clearCookie(name) {
  setCookie(name, "", -1);
}