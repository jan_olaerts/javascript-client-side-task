let c;

function initConsole(){
  c = document.getElementById("console")
  const clearBtn = document.querySelector("#consoleClear");
  clearBtn.addEventListener("click", consoleClear);
}

function consolePrint(text){
  text += "<br/>";
  c.innerHTML += text;
}

function consoleClear(){
  c.innerHTML = "";
}

function getConsole(){
  return c;
}

function addLine(line) {
  line = "<p>" + line + "</p>"; 
  c.innerHTML += line;
  c.scrollTop = c.scrollHeight;
}

function addImage(src, width) {
  c.innerHTML += `<img src=${src} width=${width}/>`;
}

window.addEventListener("load", initConsole);