function askStringNoNumbers(input) {
  const regex = /^([^0-9]*)$/;
  return regex.test(input);
}

function askStringNoLetters(input) {
  const regex = /^([^a-zA-Z]*)$/;
  return regex.test(input);
} 

function askZipCode(input) {
  const regex = /^\d{4}$/;
  return regex.test(input);
}

function checkDate(input) {
  let date = input.split("/");
  date = `${date[2]}-${date[1]}-${date[0]}`;
  ISODate = new Date(date).toISOString();
}

function checkDateNotLaterThenToday(input) {
  let date = input.split("/");
  date = `${date[2]}-${date[1]}-${date[0]}`;
  const today = new Date();
  const dateToCheck = new Date(date);

  return today > dateToCheck;
}