const btn13 = document.querySelector("#btn13");
const formDiv = document.querySelector("#formDiv");
let task11Selected = false;
const formTitle = formDiv.querySelector("h1");

const form = document.querySelector("form");
const lnError = document.querySelector("#lnError");
const fnError = document.querySelector("#fnError");
const streetError = document.querySelector("#streetError");
const zipError = document.querySelector("#zipError");
const cityError = document.querySelector("#cityError");
const emailError = document.querySelector("#emailError");
const bdError = document.querySelector("#bdError");
const mailRegex = /^([a-zA-Z\d_\.\-])+\@(([a-zA-Z\d\-])+\.)+([a-zA-Z\d]{2,4})$/;
const dateRegex = /^\d{2}[/]\d{2}[/]\d{4}$/;
const resetBtn = document.querySelector("#resetBtn");

function getFields() {
  const lastName = form.lastname.value;
  const firstName = form.firstname.value;
  const street = form.street.value;
  const number = form.number.value;
  const zipcode = form.zipcode.value;
  const city = form.city.value;
  const email = form.email.value;
  const birthday = form.birthday.value;

  return [lastName, firstName, street, number, zipcode, city, email, birthday];
}

const inputs = form.querySelectorAll("input");
inputs.forEach(input => {
  input.addEventListener("keyup", (e) => {

    validate(e, ...getFields());
  })
})

form.submitBtn.addEventListener("click", (e) => {
  e.preventDefault();

  let isValid = validate(e, ...getFields());
  if (task12Selected && isValid) {
    form.submit();
  }
})

function validate(e, lastName, firstName, street, number, zipcode, city, email, birthday) {
  let isValid = true;

  if (lastName.length === 0 || !askStringNoNumbers(lastName)) {
    isValid = false;
    form.lastname.setAttribute("class", "error");
    lnError.innerText = "*Enter a valid last name!";
  } else {
    form.lastname.classList.remove("error");
    lnError.innerText = "";
  }

  if (firstName.length === 0 || !askStringNoNumbers(firstName)) {
    isValid = false;
    form.firstname.setAttribute("class", "error");
    fnError.innerText = "*Enter a valid first name!";
  } else {
    form.firstname.classList.remove("error");
    fnError.innerText = "";
  }

  if (!askStringNoNumbers(street)) {
    isValid = false;
    form.street.setAttribute("class", "error");
    streetError.innerText = "*Enter a valid street!";
  } else {
    form.street.classList.remove("error");
    streetError.innerText = "";
  }

  if (zipcode.length > 0 && !askZipCode(zipcode)) {
    isValid = false;
    form.zipcode.setAttribute("class", "error");
    zipError.innerText = "*Enter a valid zipcode!";
  } else {
    form.zipcode.classList.remove("error");
    zipError.innerText = "";
  }

  if (!askStringNoNumbers(city)) {
    isValid = false;
    form.city.setAttribute("class", "error");
    cityError.innerText = "*Enter a valid city!";
  } else {
    form.city.classList.remove("error");
    cityError.innerText = "";
  }

  if(email.length === 0 || !mailRegex.test(email)) {
    isValid = false;
    form.email.setAttribute("class", "error");
    emailError.innerText = "*Enter a valid email";
  } else {
    form.email.classList.remove("error");
    emailError.innerText = "";
  }

  if (birthday.length === 0 || !dateRegex.test(birthday) || !checkDateNotLaterThenToday(birthday)) {
    isValid = false;
    form.birthday.setAttribute("class", "error");

    if(birthday.length === 10 && !checkDateNotLaterThenToday(birthday)) bdError.innerText = "*Birthday cannot be later than today!";
    else bdError.innerText = "*Enter a valid birthday";

  } else {
    checkDate(birthday);
    form.birthday.classList.remove("error");
    bdError.innerText = "";
  }

  if (task14bSelected && isValid) {
    for(el of form.elements) {
      if(el.type === "text") {
        setCookie(el.name, el.value, 0, 0, 5);
      }
    }
  }

  return isValid;
}

resetBtn.addEventListener("click", (e) => {
  form.reset();

  for(el of form.elements) {
    el.classList.remove("error");
  }

  const errors = [lnError, fnError, streetError, zipError, cityError, emailError, bdError];
  errors.forEach(error => error.innerText = "");
})

btn13.addEventListener("click", () => {
  task11Selected = !task11Selected;
  
  if (task11Selected) {
    task12Selected = false;
    task14bSelected = false;
    
    formDiv.style.display = "block";
    formTitle.innerText = "Task 11";
    form.removeAttribute("method");
    form.removeAttribute("action");
    cookieBtn.style.display = "none";
    addLine("Showing task 11");
  } else {
    formDiv.style.display = "none";
  }
})