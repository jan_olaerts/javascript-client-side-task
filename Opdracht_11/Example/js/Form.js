function validate(e) {
  e.preventDefault();
  let valid = true;
  const firstNameField = document.querySelector("#firstName");
  const lastNameField = document.querySelector("#lastName");

  if (firstNameField.value.length == 0) {
    valid = false;
    firstNameField.setAttribute("class", "error");
  } else {
    firstNameField.removeAttribute("class");
  }

  if (lastNameField.value.length == 0) {
    valid = false;
    lastNameField.setAttribute("class", "error");
  } else {
    lastNameField.removeAttribute("class");
  }

  if (!valid) {
    console.log("not valid");
    const errorElement = document.querySelector("#error");
    errorElement.innerText += "Enter your first name and last name!";
  }
}

function init() {
  const form = document.querySelector("#myForm");
  form.addEventListener("submit", validate);
}

window.addEventListener("load", init);