const btn6 = document.querySelector("#btn6");

btn6.addEventListener("click", () => {
  addLine("Reload the page and resize the browser to see the text in the console");
})

window.addEventListener("resize", () => {
  addLine(`New browser dimensions:
  Width: ${window.innerWidth} px\nHeight: ${window.innerHeight} px`)
})

window.addEventListener("load", () => {
  addLine("Page loaded");
  addLine("<br/>");
})