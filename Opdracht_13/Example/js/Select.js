function select(e) {
  const form = document.querySelector("#myForm");
  const selection = document.querySelector("#selection");
  const option = selection.options[selection.selectedIndex];
  alert("Your choice: " + option.value);
  form.submit();
}

function init() {
  const selection = document.querySelector("#selection");
  selection.addEventListener("change", select);
}

window.addEventListener("load", init);