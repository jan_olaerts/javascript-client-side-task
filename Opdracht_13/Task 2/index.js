const btn16 = document.querySelector("#btn16");
const task13bDiv = document.querySelector("#task13bDiv");
let task13bSelected = false;

const daySelection = document.querySelector("#days");
const monthSelection = document.querySelector("#months");
const yearSelection = document.querySelector("#years");

const months = ["January", "Februari", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December"];

for (let i = 1; i < 32; i++) {
  const option = document.createElement("option");
  option.setAttribute("value", i);
  option.innerText = i;
  daySelection.appendChild(option);
}

for (let i = 0; i < 12; i++) {
  const option = document.createElement("option");
  option.setAttribute("value", months[i]);
  option.innerText = months[i];
  monthSelection.appendChild(option);
}


for (let i = 1920; i < 2021; i++) {
  const option = document.createElement("option");
  option.setAttribute("value", i);
  option.innerText = i;
  yearSelection.appendChild(option);
}

const day28 = daySelection.querySelectorAll("option")[27];
const day29 = daySelection.querySelectorAll("option")[28];
const day30 = daySelection.querySelectorAll("option")[29];
const day31 = daySelection.querySelectorAll("option")[30];

function reset() {
  Array.from(daySelection).forEach(i => i.disabled = false);
  Array.from(monthSelection).forEach(i => i.disabled = false);
}

function checkLeapYear(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function check(e) {
  reset();

  const day = daySelection.value;
  const month = monthSelection.value;
  const year = yearSelection.value;

  if (day == 31) {
    Array.from(monthSelection).forEach(i => {
      if (i.innerText === months[1] || i.innerText === months[3] || i.innerText === months[5]
          || i.innerText === months[8] || i.innerText === months[10]) {
            i.disabled = true;
          }
    });
  }

  if (day == 30) {
    monthSelection[1].disabled = true;
  }
  
  if (month === months[3] || month === months[5] || 
      month === months[8] || month === months[9]) {
    day31.disabled = true;
  }

  if (month === months[1]) {
    if (checkLeapYear(year)) {
      day30.disabled = true;
      day31.disabled = true;
    } else {
      day29.disabled = true;
      day30.disabled = true;
      day31.disabled = true;
    }
  }

  if((day == 29 || day == 30 || day == 31) && month == months[1] && !checkLeapYear(year)) {
    daySelection.selectedIndex = 27;
  }
}

daySelection.addEventListener("change", check);
monthSelection.addEventListener("change", check);
yearSelection.addEventListener("change", check);

btn16.addEventListener("click", () => {
  task13bSelected = !task13bSelected;

  if(task13bSelected) {
    addLine("Showing task 13 (2/2)");
    task13bDiv.style.display = "block";
  } else {
    task13bDiv.style.display = "none";
  }
})