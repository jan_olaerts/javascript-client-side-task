const task13aDiv = document.querySelector("#task13aDiv");
const colorForm = document.querySelector("#colorForm");
const btn15 = document.querySelector("#btn15");
let task13aSelected = false;

btn15.addEventListener("click", () => {
  task13aSelected = !task13aSelected;

  if(task13aSelected) {
    addLine("Showing task 13 (1/2)");
    task13aDiv.style.display = "block";

    colorForm.addEventListener("change", e => {
      const color = colorForm.selection.value;
      body.style.backgroundColor = color;
    })
  } else {
    task13aDiv.style.display = "none";
  }
})